Tere tulemast lugema seda tekstikest!

Selle programmi loojad on Robert ja Anna. Kui tekib küsimusi, siis võib julgelt küsida.



##TO-DO list##
* Erineva kiirusega noodid (titi, ta-a) + pausid?
* Lisakujundus (teistsugune kursor, midagi liidese peale).

---
* Klaviatuuri kasutamine hiire asemel.
* Programm salvestab loodud lugusid eraldi faili.
* Programm suudab failist lugeda noote ja seda ette mängida.
* Helistiku vahetamine.